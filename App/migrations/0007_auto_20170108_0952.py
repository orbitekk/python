# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0006_auto_20170108_0106'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='premium',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='post',
            name='published',
            field=models.BooleanField(default=True),
        ),
    ]
