from django.conf.urls import patterns, url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^$', views.post_list, name='post_list'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^manage/$', views.manage, name='manage'),
    url(r'^post/delete/(?P<pk>[0-9]+)/$', views.post_delete, name='post_delete'),
    url(r'^logout/$', views.logoutUser, name='logoutUser'),
    url(r'^accounts/register/complete', views.registerComplete, name='registerComplete'),
    url(r'^accounts/', include('registration.backends.simple.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
