from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post
from .forms import PostForm
from .forms import ContactForm
from django.contrib.auth import logout
from django.core.mail import EmailMessage
from django.template import Context
from django.template.loader import get_template

def manage(request):
    posts = Post.objects.all()
    return render(request, 'blog/manage.html', {'posts': posts})

def post_delete(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('manage')

def post_list(request):
    if request.user.is_authenticated():
        posts = Post.objects.all().filter(published=True).order_by('-published_date')
    else:
        posts = Post.objects.all().filter(published=True, premium=False).order_by('-published_date')
    count = posts.count()
    pageSize = 2
    page = int(request.GET.get('page',1))
    offset = (page - 1) * pageSize
    isNextpage = count / float(pageSize) > page
    isPreviousPage = page > 1
    nextPage = page + 1
    previousPage = page - 1
    return render(request, 'blog/post_list.html', {'page':offset,'posts': posts[offset:offset + pageSize], 'isNextPage': isNextpage, 'isPreviousPage': isPreviousPage, 'nextPage': nextPage, 'previousPage': previousPage})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.image = form.cleaned_data['image']
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.image = form.cleaned_data['image']
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})

def contact(request):
    form_class = ContactForm

    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name'
            , '')
            contact_email = request.POST.get(
                'contact_email'
            , '')
            form_content = request.POST.get('content', '')

            # Email the profile with the
            # contact information
            template = get_template('contact_template.txt')
            context = Context({
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            })
            content = template.render(context)

            email = EmailMessage(
                "New contact form submission",
                content,
                "Your website" +'',
                ['youremail@gmail.com'],
                headers = {'Reply-To': contact_email }
            )
            email.send()
            return redirect('contact')

    return render(request, 'blog/contact.html', {'form': form_class})

def logoutUser(request):
    logout(request)
    return redirect('/')

def registerComplete(request):
    return redirect('/')